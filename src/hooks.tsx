import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
// @ts-ignore
import { AppDispatch, RootState } from './store/store';

type DispatchFunc = () => AppDispatch;
export const useAppDispatch: DispatchFunc = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
